# Algolia Assignment - Instant Search Demo

## Instructions

### Subject

Create an automated procedure for deploying and updating this project, with graceful
handling of failures.
It would be nice to provide a way to test your procedure locally (for example with a
virtual machine), and a document with instructions on how to use your procedure.

### Requirements

No particular technology is required, you can choose to use whatever you like.

Your procedure should be resilient to errors and leave the installation in a working state
even if something bad happens during an update.

## Architecture

The Instant Search Demo is a sample project of an [Algolia](http://www.algolia.com)
Instant-Search result page on an e-commerce website.
Algolia is a Search API that provides a hosted full-text, numerical and faceted search.

This application is solely composed of static HTML/CSS assets, which thus can be directly served as is.

Thus, those assets are stored in a S3 bucket which is exposed behind a CloudFront distribution
(see architecture diagram below).

In order to handle rolling update requirement, this architecture leverage
[Lambda@Edge](https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/lambda-at-the-edge.html)
to redirect traffic between two origin:

- `old` origin which expose the current application version
- `new` origin which host the new application version content at deployment time

This decision is taken given the value (`old` or `new`) stored
in `/cloudfront-rolling-update/origin/current-target` SSM parameter.
It is possible to force the origin target by setting the `X-CloudFront-Force-Current-Target=<old|new>`
cookie in your web browser to target `old` or `new` origin whatever SSM parameter value configured.

This component is detailed in a
[dedicated documentation](./infrastructure/terraform/modules/cloudfront-s3-webstatic/lambda/README.md)

![Instant Search Demo - AWS Architecture](./docs/images/instant-search-demo-architecture.png "Instant Search Demo - AWS Architecture")

During a deployment of a new application version, ansible will follow these steps:

1. Ensure that CloudFront already forward traffic to old target origin
   (ie. check that SSM parameter `/cloudfront-rolling-update/origin/current-target` is defined to `old`)
2. Deploy Instant Search Demo application to S3 Bucket `new.instant-search-demo.algolia`
   (this version is not exposed at this time to end users
3. Ask operator to ensure that new deployed version is working correctly.
   - You can set `X-CloudFront-Force-Current-Target=new` cookie in browser to force CloudFront target new origin
   - You can rely on [cypress acceptance tests](./instant-search-demo/cypress/integration/instant_search_demo_spec.js)
     defined in instant-search-demo source directory
4. What's next?
   - If there is any problem with new version => **STOP**
   - Otherwise we can continue to finish rolling update
5. Configure CloudFront Parameter to forward traffic to new target origin
   (ie. switch SSM parameter `/cloudfront-rolling-update/origin/current-target` to `new`)
6. Deploy Instant Search Demo application to S3 Bucket `old.instant-search-demo.algolia`
   (this version is not exposed at this time to end users)
7. Configure CloudFront Parameter to forward traffic to old target origin (this is our current version origin)
   => **END**

Disclaimer: At now, this implement is not optimal in terms of performance (about 500ms for TTFB without cache).
Optimisation should be applied, such as limiting AWS SSM access from cloudfront_rolling_update Lambda@Edge.

## Quick Start

This section aims to understand at a glance how to reach a functional Instant Search Demo application!
It assumes that the prerequisites presented further have been met.
For further details, read on the next sections.

### Create required terraform backend

WARNING: This required that you deploy on [provided AWS account](./docs/AWS-SSO-ACCOUNT.md), as bucket name would
be already taken, otherwise you should:

- change `terraform_backend_bucket_name` variable in [ansible configuration](infrastructure/ansible/group_vars/all/main.yaml)
- change `terraform_backend_table_name` variable in [ansible configuration](infrastructure/ansible/group_vars/all/main.yaml)
- update accordingly `bucket` and `dynamodb_table` variables in [terraform backend configuration](infrastructure/terraform/config/backend/master.backend.hcl)

```shell
cd infrastructure/ansible

ansible-playbook create-terraform-backend.yaml
# TASK [terraform_backend : Create terraform backend S3 bucket 'terraform-algolia-brabant'] ***
# ok: [localhost -> localhost]

# TASK [terraform_backend : Create terraform backend DynamoDB table 'terraform-algolia-brabant'] ***
# ok: [localhost -> localhost]
```

### Initialize terraform configuration

```shell
cd infrastructure/terraform
terraform -chdir=stacks/instant-search-demo init -backend-config=$(pwd)/config/backend/master.backend.hcl
```

### Set up Instant Search Demo infrastructure on AWS

WARNING: This required that you deploy on [provided AWS account](./docs/AWS-SSO-ACCOUNT.md), otherwise you should:

- remove those `var` arguments to keep the default CloudFront distribution domain name
- or replace `dns_zone_name` and `target_domain_name` with your own configuration

```shell
cd infrastructure/terraform
terraform -chdir=stacks/instant-search-demo apply \
  -var 'enable_custom_dns=true' \
  -var 'dns_zone_name=algolia.brabant.dev' \
  -var 'target_domain_name=instant-search-demo.algolia.brabant.dev'

[...]

Apply complete! Resources: 5 added, 0 changed, 0 destroyed.

  Outputs:

cloudfront_distribution_arn = "arn:aws:cloudfront::131605690760:distribution/EYAQ593V9C36O"
cloudfront_distribution_domain_name = "instant-search-demo.algolia.brabant.dev"
cloudfront_distribution_id = "EYAQ593V9C36O"
s3_website_new_version_bucket_arn = "arn:aws:s3:::new.instant-search-demo.algolia20210723065019682300000002"
s3_website_old_version_bucket_arn = "arn:aws:s3:::old.instant-search-demo.algolia20210723065019682600000003"
zzz_message = <<EOT
  The CloudFront distribution has been successfully deployed and can be accessed at:
  https://instant-search-demo.algolia.brabant.dev

  You might get an AccessDenied error access if you deployed this stack for the first time. This error occurs when target
  bucket origin is empty; thus CloudFront complains that it cannot return index.html.

  => You should now move to ansible side to deploy instant-search-demo application

EOT
```

### Prepare Instant Search Demo files for deployment

In order to ease observation of rolling update phase, you can leverage the following make targets in order to publish a
slightly transformed application with search bar colored in blue, green or red.

- Include blue CSS style override: `make instant-search-demo.prepare-blue`
- Include green CSS style override: `make instant-search-demo.prepare-green`
- Include red CSS style override: `make instant-search-demo.prepare-red`

For this quick start documentation, let's start with `blue`

```shell
## Go to the project root directory
make instant-search-demo.prepare-blue

Prepare Instant Search Demo distribution

> instant-search-demo@2.0.0 dist
> mkdir -p dist/ && cp -rf assets index.html search.js dist/

Copy Instant Search Demo distribution to ansible target input directory for deploy:
(FROM) instant-search-demo/dist/ ==> (TO) infrastructure/ansible/files/instant-search-demo
```

### Deploy Instant Search Demo application on AWS

```shell
cd infrastructure/ansible
ansible-playbook deploy-instant-search-demo.yaml

[...]

PLAY RECAP ***********************************************************************
localhost                  : ok=14   changed=4    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
```

### Run acceptance tests to ensure deployment went well

In order to ease acceptance testing of newly deployed application version,
we can leverage [Cypress](https://www.cypress.io/) testing suite which automate basic search scenario.

You need to set `CYPRESS_BASE_URL` environment variable to the CloudFront distribution URL that has been printed before.

```shell
cd ./instant-search-demo
npm install --dev
CYPRESS_BASE_URL="https://<cloudfront_distribution_domain_name>" \
CYPRESS_SEARCH_BAR_COLOR="BLUE" \
npx cypress run --no-exit --headed --spec "cypress/integration/instant_search_demo_spec.js"
  (Run Starting)

====================================================================================================

  (Run Starting)

  ┌────────────────────────────────────────────────────────────────────────────────────────────────┐
  │ Cypress:    8.0.0                                                                              │
  │ Browser:    Electron 89                                                                        │
  │ Specs:      1 found (instant_search_demo_spec.js)                                              │
  │ Searched:   cypress/integration/instant_search_demo_spec.js                                    │
  └────────────────────────────────────────────────────────────────────────────────────────────────┘


────────────────────────────────────────────────────────────────────────────────────────────────────

  Running:  instant_search_demo_spec.js                                                     (1 of 1)


  Assertions
    Instant Search Demo
      ✓ should return Bokoblin amiibo figure, when searching Bokoblin (5140ms)
      ✓ should display search bar in "BLUE" color (872ms)


  2 passing (6s)

```

## Getting Started

For a specific documentation regarding development of [Instant Search Demo](./instant-search-demo) application, please
read the dedicated documentation in the application subdirectory

### Requirements

#### Docker

In order to run [Instant Search Demo](./instant-search-demo) application locally, you can leverage Docker setup.

You will need to setup a [local install of Docker](https://docs.docker.com/get-docker/) by following the dedicated documentation.
You will also need [Docker Compose](https://docs.docker.com/compose/install/) tool installed.

You can check your install the following way:

```shell
docker --version
# Docker version 20.10.6, build 370c289

docker compose version
# Docker Compose version 2.0.0-beta.1
```

#### AWS

This project target AWS cloud provider for deployment, you will need access to an AWS account to deploy application to.
You will require credentials to allow access to AWS account.

see: <https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html>

You can leverage already provisioned AWS account by authenticated through AWS SSO.
Please find more information in [this documentation](./docs/AWS-SSO-ACCOUNT.md).

#### Ansible

You will need ansible to initialize terraform backend. Also ansible will be used to deploy a new application version.
Ansible 4.0+ required a Python 3.8+ working environment. You can leverage [pyenv](https://github.com/pyenv/pyenv) tool
to ease management of multiple Python versions.

You can then basically install ansible in your Python environment:

```shell
cd infrastructure/ansible

# (optional) setup a local virtualenv
python3 -m venv .venv
source .venv/bin/activate

pip install -r requirements.txt
```

For other installation alternatives, please follow this guide:
<https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html>

#### Terraform

This project rely on terraform for infrastructure setup, you would need to install latest available terraform version v1.0+
<https://learn.hashicorp.com/tutorials/terraform/install-cli>

You can also leverage [tfenv](https://github.com/tfutils/tfenv) to manage multiple terraform version installed locally.
A [.terraform-version](./infrastructure/terraform/.terraform-version) file has been defined to automatically select the
right supported terraform version when running terraform from any child directory of `.terraform-version` file.

In order to use terraform, you will first need to create a [terraform backend](https://www.terraform.io/docs/language/settings/backends/s3.html).
After setting up your AWS credentials in current terminal:

WARNING: This required that you deploy on [provided AWS account](./docs/AWS-SSO-ACCOUNT.md), otherwise you should:

- change `terraform_backend_bucket_name` variable in [ansible configuration](infrastructure/ansible/group_vars/all/main.yaml)
- change `terraform_backend_table_name` variable in [ansible configuration](infrastructure/ansible/group_vars/all/main.yaml)
- update accordingly `bucket` and `dynamodb_table` variables in [terraform backend configuration](infrastructure/terraform/config/backend/master.backend.hcl)

```shell
cd infrastructure/ansible
ansible-playbook create-terraform-backend.yaml
# TASK [terraform_backend : Create terraform backend S3 bucket 'terraform-algolia-brabant'] ***
# ok: [localhost -> localhost]

# TASK [terraform_backend : Create terraform backend DynamoDB table 'terraform-algolia-brabant'] ***
# ok: [localhost -> localhost]
```

You will then need to initialize terraform configuration:

```shell
cd infrastructure/terraform
terraform -chdir=stacks/instant-search-demo init -backend-config=$(pwd)/config/backend/master.backend.hcl
```

## Usage

### Setup AWS Infrastructure

In order to set up Instant Search Demo infrastructure on AWS, follow the below instructions:

```shell
cd infrastructure/terraform
terraform -chdir=stacks/instant-search-demo apply

[...]

Apply complete! Resources: 5 added, 0 changed, 0 destroyed.

  Outputs:

cloudfront_distribution_arn = "arn:aws:cloudfront::131605690760:distribution/EYAQ593V9C36O"
cloudfront_distribution_domain_name = "instant-search-demo.algolia.brabant.dev"
cloudfront_distribution_id = "EYAQ593V9C36O"
s3_website_new_version_bucket_arn = "arn:aws:s3:::new.instant-search-demo.algolia20210723065019682300000002"
s3_website_old_version_bucket_arn = "arn:aws:s3:::old.instant-search-demo.algolia20210723065019682600000003"
zzz_message = <<EOT
  The CloudFront distribution has been successfully deployed and can be accessed at:
  https://instant-search-demo.algolia.brabant.dev

  You might get an AccessDenied error access if you deployed this stack for the first time. This error occurs when target
  bucket origin is empty; thus CloudFront complains that it cannot return index.html.

  => You should now move to ansible side to deploy instant-search-demo application

EOT
```

As stated in output `zzz_message`,
you might get the below `AccessDenied` error access if you deployed this stack for the first time.
This error occurs when target bucket origin is empty; thus CloudFront complains that it cannot return `index.html`.

```xml
<Error>
    <Code>AccessDenied</Code>
    <Message>Access Denied</Message>
    <RequestId>XTKNY3XJ1A9F4RDP</RequestId>
    <HostId>nabRCjja36kMYs3Lmzc6B9GeAUHuHMDclKh598eY9org...</HostId>
</Error>
```

=> You should now move to ansible side to deploy instant-search-demo application

Note: By default, CloudFront distribution is configured to only exposed default domain name ended in `cloudfront.net`.

You can enable custom DNS configuration by providing:

- `enable_custom_dns` to `true` (default: `false`)
- `dns_zone_name` to the existing hosted zone name (default: `algolia.brabant.dev`
- `target_domain_name` to the target domain name (default: `instant-search-demo.algolia.brabant.dev`)

```shell
cd infrastructure/terraform
terraform -chdir=stacks/instant-search-demo apply \
    -var 'enable_custom_dns=true' \
    -var 'dns_zone_name=algolia.brabant.dev' \
    -var 'target_domain_name=instant-search-demo.algolia.brabant.dev'
```

### Deploy Instant Search Demo

From now, Instant Search Demo application is not published in an artefact manager. Thus you will need to `prepare`
files for deployment, which consists in copying static files into `infrastructure/ansible/files/instant-search-demo`
ansible directory.

```shell
make instant-search-demo.prepare

Prepare Instant Search Demo distribution

> instant-search-demo@2.0.0 dist
> mkdir -p dist/ && cp -rf assets index.html search.js dist/

Copy Instant Search Demo distribution to ansible target input directory for deploy:
(FROM) instant-search-demo/dist/ ==> (TO) infrastructure/ansible/files/instant-search-demo
```

In order to ease observation of rolling update phase, you can leverage the following make targets in order to publish a
slightly transformed application with search bar colored in blue, green or red.

- Include blue CSS style override: `make instant-search-demo.prepare-blue`
- Include green CSS style override: `make instant-search-demo.prepare-green`
- Include red CSS style override: `make instant-search-demo.prepare-red`

Then, in order to deploy Instant Search Demo application on AWS, follow the below instructions:

```shell
cd infrastructure/ansible
ansible-playbook deploy-instant-search-demo.yaml

PLAY [localhost] *****************************************************************

TASK [instant_search_demo : Retrieve all AWS CloudFront distributions informations from AWS account] ******************************************************************
ok: [localhost]

TASK [instant_search_demo : Filter AWS CloudFront instant_search_demo distribution by tags] ***************************************************************************
ok: [localhost]

TASK [instant_search_demo : Retrieve instant_search_demo CloudFront distribution domain name and aliases] *************************************************************
ok: [localhost]

TASK [instant_search_demo : Retrieve instant_search_demo CloudFront distribution URL] *********************************************************************************
ok: [localhost]

TASK [instant_search_demo : Retrieve all AWS S3 buckets informations from AWS account] ********************************************************************************
ok: [localhost]

TASK [instant_search_demo : Retrieve instant_search_demo old and new target bucket name] ******************************************************************************
ok: [localhost]

TASK [instant_search_demo : Ensure that CloudFront already forward traffic to old target origin] **********************************************************************
ok: [localhost -> localhost]

TASK [instant_search_demo : Create environment file from template into artifact directory] ****************************************************************************
ok: [localhost -> localhost]

TASK [instant_search_demo : Deploy Instant Search Demo application to S3 Bucket 'new.instant-search-demo.algolia20210723065019682300000002'] **************************
changed: [localhost -> localhost]

TASK [instant_search_demo : No change in new application version, end here.] ******************************************************************************************
skipping: [localhost]

TASK [instant_search_demo : ansible.builtin.pause] ********************************************************************************************************************
[instant_search_demo : ansible.builtin.pause]
Ensure that new deployed version is working correctly.
You can set 'X-CloudFront-Force-Current-Target=new' cookie in browser to force CloudFront target new origin

You can also leverage cypress acceptance tests defined in instant-search-demo source directory:

    CYPRESS_BASE_URL="https://instant-search-demo.algolia.brabant.dev" \
    CYPRESS_SEARCH_BAR_COLOR="DEFAULT | BLUE | GREEN | RED" \
    CYPRESS_FORCE_CLOUDFRONT_ORIGIN="new" \
    npx cypress run --no-exit --headed --spec "cypress/integration/instant_search_demo_spec.js"

Do you want to continue? (yes/no)
: yes
ok: [localhost -> localhost]

TASK [instant_search_demo : ansible.builtin.assert] *******************************************************************************************************************
ok: [localhost -> localhost] => {
    "changed": false,
    "msg": "Rolling update continue after user validation"
}

TASK [instant_search_demo : Configure CloudFront Parameter to forward traffic to new target origin] *******************************************************************
changed: [localhost -> localhost]

TASK [instant_search_demo : Deploy Instant Search Demo application to S3 Bucket 'old.instant-search-demo.algolia20210723065019682600000003'] **************************
changed: [localhost -> localhost]

TASK [instant_search_demo : Configure CloudFront Parameter to forward traffic to old target origin] *******************************************************************
changed: [localhost -> localhost]

PLAY RECAP ***********************************************************************
localhost                  : ok=14   changed=4    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
```

As you can see in above ansible output, instant-search-demo application will be rolled out following multiple steps.

At some time, it will ask you to check if new deployed version is functional before rolling it out to end users.
At this moment, application has only been deployed on `new.instant-search-demo.algolia.brabant` bucket whereas
 users are still routed to `old.instant-search-demo.algolia.brabant`.

In order to check the new deployed version, you can set `X-CloudFront-Force-Current-Target` cookie to `new` value which
will force CloudFront to route your traffic to the `new.instant-search-demo.algolia.brabant` origin.
Copy `cloudfront_distribution_domain_name` from your terraform outputs in your web browser
to access Instant Search Demo application.
About how to manage cookies in Google Chrome browser: <https://developer.chrome.com/docs/devtools/storage/cookies/>

Note: In order to ease manual acceptance testing, please read the next section about Cypress testing

For instance, if we choose the `blue` application flavor, we should see this screen:
![Instant Search Demo - Blue Flavor](./docs/images/instant-search-demo-blue-flavor.png)

Then, you have the following options:

- either new application fulfil your expectations, thus you can answer `yes` to continue the rolling update.
- or new application version does not suit your expectations, thus you can answer `no` to cancel the rolling update.
In this later case, your users will remain on current application version without noticing that update was aborted.

Note: by default, ansible will only copy files to remote S3 bucket if date is more recent and/or file size changed.
Also, play will be ended prematurely if no changes has been detected in new application version during S3 sync.

=> You can add `force` extra argument to ansible command line in order to deploy the application anyway.

```shell
cd infrastructure/ansible
ansible-playbook deploy-instant-search-demo.yaml -e force=true
```

#### Automate Acceptance Testing

In order to ease acceptance testing of newly deployed application version,
we can leverage [Cypress](https://www.cypress.io/) testing suite which automate basic search scenario.

You can run these tests the following way:

```shell
cd ./instant-search-demo
npm install --dev
npx cypress run --no-exit --headed --spec "cypress/integration/instant_search_demo_spec.js"

====================================================================================================

  (Run Starting)

  ┌────────────────────────────────────────────────────────────────────────────────────────────────┐
  │ Cypress:    8.0.0                                                                              │
  │ Browser:    Electron 89                                                                        │
  │ Specs:      1 found (instant_search_demo_spec.js)                                              │
  │ Searched:   cypress/integration/instant_search_demo_spec.js                                    │
  └────────────────────────────────────────────────────────────────────────────────────────────────┘


────────────────────────────────────────────────────────────────────────────────────────────────────

  Running:  instant_search_demo_spec.js                                                     (1 of 1)


  Assertions
    Instant Search Demo
      ✓ should return Bokoblin amiibo figure, when searching Bokoblin (5140ms)
      ✓ should display search bar in "DEFAULT" color (872ms)


  2 passing (6s)

```

As we provided [--headed](https://docs.cypress.io/guides/guides/command-line#cypress-run-headed) options, you should
see web browser appearing with running test suite in it.

![Instant Search Demo under cypress acceptance testing suite](./docs/images/instant-search-demo-cypress-tests.png "Instant Search Demo - Cypress Testing")

By default, cypress run against local application instance targeting base_url <http://localhost:8080>.

You can override target application by defining `CYPRESS_BASE_URL` environment variable.
Also you can check search bar color flavor by defining `CYPRESS_SEARCH_BAR_COLOR` which can take values among:

- `DEFAULT` (or `WHITE`) which represent `#ffffff` search bar color
- `BLUE` which represent `#5568ff` search bar color
- `GREEN` which represent `#00e676` search bar color
- `RED` which represent `#ff426b` search bar color

Finally, you can define `CYPRESS_FORCE_CLOUDFRONT_ORIGIN` environment variable with `old` or `new` value which will
set up `X-CloudFront-Force-Current-Target` cookie in order to force serving one or another CloudFront origin.

```shell
CYPRESS_BASE_URL="https://<cloudfront_distribution_domain_name>" \
CYPRESS_SEARCH_BAR_COLOR="GREEN" \
CYPRESS_FORCE_CLOUDFRONT_ORIGIN="new" \
npx cypress run --no-exit --headed --spec "cypress/integration/instant_search_demo_spec.js"
```

## Local

As the chosen technical stack relies mainly on cloud native services, full local deployment is unfortunately difficult
to achieve.
Anyway solution exists to reproduce partly AWS environment locally, among us [localstack](https://localstack.cloud) is
a quite promising one.
Thus, you should start localstack container right now if you are interested in running stack locally.

Disclaimer: This is not fully functional due to localstack bugs and lack of support for recent features.

```shell
docker compose up localstack -d
```

This will expose amongst others, the port 4566 that can be used this way with aws cli:

```shell
aws --endpoint-url=http://localhost:4566 s3 ls
```

Note: Localstack features for CloudFront are only available in pro version, which can be contracted in a 15 days trial.

See: <https://localstack.cloud/features/>

You should configure `LOCALSTACK_API_KEY` environment variable in [Docker Compose](./docker-compose.yml) with API key
retrieve from your [localstack](https://localstack.cloud) account

### Instant Search Demo

You can run a containerised version of [Instant Search Demo](./instant-search-demo) application, this way:

```shell
docker compose up instant-search-demo -d --build
```

This will build application Docker container and run it locally: <http://localhost:8080>

### Terraform Backend

By running `aws --endpoint-url=http://localhost:4566 s3 ls` you should notice that `terraform-algolia-brabant` s3
bucket has already been provisioned thanks to [localstack startup script](./localstack/setup-terraform-backend.sh)

Thus, you don't need to run `ansible-playbook create-terraform-backend.yaml` in local setup.

You will then need to (re)initialize your terraform backend:

```shell
cd infrastructure/terraform
terraform -chdir=stacks/instant-search-demo init -reconfigure -backend-config=$(pwd)/config/backend/local.backend.hcl
```

Finally, you will need to override
[terraform providers configuration](./infrastructure/terraform/stacks/instant-search-demo/providers.tf) code
in order to define the local endpoint urls.

```hcl-terraform
provider "aws" {
  region = var.region

  skip_credentials_validation = true
  skip_requesting_account_id  = true
  skip_metadata_api_check     = true

  s3_force_path_style         = true

  endpoints {
    cloudfront     = "http://localhost:4566"
    iam            = "http://localhost:4566"
    lambda         = "http://localhost:4566"
    route53        = "http://localhost:4566"
    s3             = "http://localhost:4566"
    secretsmanager = "http://localhost:4566"
    ssm            = "http://localhost:4566"
    sts            = "http://localhost:4566"
  }
}

provider "aws" {
  alias  = "cloudfront-region"
  region = "us-east-1"

  skip_credentials_validation = true
  skip_requesting_account_id  = true
  skip_metadata_api_check     = true

  s3_force_path_style         = true

  endpoints {
    cloudfront     = "http://localhost:4566"
    iam            = "http://localhost:4566"
    lambda         = "http://localhost:4566"
    route53        = "http://localhost:4566"
    s3             = "http://localhost:4566"
    secretsmanager = "http://localhost:4566"
    ssm            = "http://localhost:4566"
    sts            = "http://localhost:4566"
  }
}
```

### Setup AWS Infrastructure

In order to set up Instant Search Demo infrastructure locally, follow the below instructions:

```shell
cd infrastructure/terraform
terraform -chdir=stacks/instant-search-demo apply -var-file=terraform.local.tfvars

[...]

Apply complete! Resources: 5 added, 0 changed, 0 destroyed.

  Outputs:

  cloudfront_distribution_arn = "arn:aws:cloudfront::000000000000:distribution/d7b52868"
  cloudfront_distribution_domain_name = "d7b52868.cloudfront.localhost.localstack.cloud"
  cloudfront_distribution_id = "d7b52868"
  s3_website_new_version_bucket_arn = "arn:aws:s3:::new.instant-search-demo.algolia.brabant"
  s3_website_old_version_bucket_arn = "arn:aws:s3:::old.instant-search-demo.algolia.brabant"
```

Note: As of today, the local deployment does not completely succeed. See the following section for more insights about
encountered problems.

#### Troubleshooting

At first, when trying to apply terraform stack, we run into the following error:

```shell
╷
│ Error: unable to locate cache policy by name: :
│   status code: 404, request id:
│
│   with module.cloudfront_s3_webstatic.data.aws_cloudfront_cache_policy.this,
│   on ../../modules/cloudfront-s3-webstatic/data.tf line 3, in data "aws_cloudfront_cache_policy" "this":
│    3: data "aws_cloudfront_cache_policy" "this" {
│
```

Localstack container logs seems quite explicit about this:

```shell
WARNING:localstack_ext.services.cloudfront.cloudfront_api: Not yet implemented:
Unable to find path mapping for GET /2020-05-31/cache-policy
```

To move forward, `aws_cloudfront_cache_policy` and `aws_cloudfront_origin_request_policy` datasources and resources
needs to be removed from [cloudfront-s3-webstatic](./infrastructure/terraform/modules/cloudfront-s3-webstatic) module.
The `cache_policy_id` and `origin_request_policy_id` values from `aws_cloudfront_distribution` resource should be
hardcoded with dummy values.

Then we are able to fully deploy stack (with a licensed localstack only due to CloudFront requirement).
Sadly when we try to go to distribution URL <https://d7b52868.cloudfront.localhost.localstack.cloud> here above,
a 500 error is raised with the following traces in localstack logs:

```shell
Traceback (most recent call last):
localstack_1           |   File "/opt/code/localstack/.venv/lib/python3.7/site-packages/flask/app.py", line 2070, in wsgi_app
localstack_1           |     response = self.full_dispatch_request()
localstack_1           |   File "/opt/code/localstack/.venv/lib/python3.7/site-packages/flask/app.py", line 1515, in full_dispatch_request
localstack_1           |     rv = self.handle_user_exception(e)
localstack_1           |   File "/opt/code/localstack/.venv/lib/python3.7/site-packages/flask_cors/extension.py", line 165, in wrapped_function
localstack_1           |     return cors_after_request(app.make_response(f(*args, **kwargs)))
localstack_1           |   File "/opt/code/localstack/.venv/lib/python3.7/site-packages/flask/app.py", line 1513, in full_dispatch_request
localstack_1           |     rv = self.dispatch_request()
localstack_1           |   File "/opt/code/localstack/.venv/lib/python3.7/site-packages/flask/app.py", line 1499, in dispatch_request
localstack_1           |     return self.ensure_sync(self.view_functions[rule.endpoint])(**req.view_args)
localstack_1           |   File "/opt/code/localstack/.venv/lib/python3.7/site-packages/localstack_ext/services/cloudfront/cloudfront_api.py", line 357, in fallback
localstack_1           |   File "/opt/code/localstack/.venv/lib/python3.7/site-packages/localstack_ext/services/cloudfront/cloudfront_api.py", line 369, in invoke_distribution
localstack_1           |   File "/opt/code/localstack/.venv/lib/python3.7/site-packages/localstack_ext/services/cloudfront/cloudfront_api.py", line 401, in get_matching_origin
localstack_1           |   File "/opt/code/localstack/.venv/lib/python3.7/site-packages/localstack_ext/services/cloudfront/cloudfront_api.py", line 401, in <listcomp>
localstack_1           | AttributeError: 'str' object has no attribute 'get'
```

### Deploy Instant Search Demo

You can follow the previous corresponding section which is absolutely identical for local environment.
The only difference stands in providing `local=true` extra ansible variable
in order to switch ansible module `endpoint_url` to target localstack instance.

```shell
cd infrastructure/ansible
ansible-playbook deploy-instant-search-demo.yaml -e local=true
```

Note: This step is fully functional locally provided that you followed troubleshooting previous steps
in order to achieve required infrastructure setup.

## Clean Up

### AWS

First you will remove deployed infrastructure on AWS with terraform.

```shell
cd infrastructure/terraform
terraform -chdir=stacks/instant-search-demo destroy
```

WARNING: when destroying terraform stack, the following error has many chances to occur:

```shell
│ Error: error deleting Lambda Function (lambda-rolling-update-origin): InvalidParameterValueException: Lambda was unable to delete arn:aws:lambda:us-east-1:000000000000:function:lambda-rolling-update-origin:9 because it is a replicated function. Please see our documentation for Deleting Lambda@Edge Functions and Replicas.
│ {
│   RespMetadata: {
│     StatusCode: 400,
│     RequestID: "c4afd5c7-9123-4884-a725-0f29a15c7be4"
│   },
│   Message_: "Lambda was unable to delete arn:aws:lambda:us-east-1:323321938130:function:lambda-rolling-update-origin:9 because it is a replicated function. Please see our documentation for Deleting Lambda@Edge Functions and Replicas."
│ }
│
```

This is due to CloudFront that can take through 1 hour to delete Lambda@Edge replicas,
which prevent deletion of corresponding lambda function.

=> Trying another destroy a few minutes later is generally enough to complete cleanup

For more information:

- <https://github.com/hashicorp/terraform-provider-aws/issues/1721>
- <https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/lambda-edge-delete-replicas.html>

### Terraform Backend

You can then completely remove terraform backend.

```shell
cd infrastructure/ansible
ansible-playbook destroy-terraform-backend.yaml

# TASK [terraform_backend : Destroy terraform backend S3 bucket 'terraform-algolia-brabant'] ****************************************************************************
# changed: [localhost -> localhost]

# TASK [terraform_backend : Destroy terraform backend DynamoDB table 'terraform-algolia-brabant'] ***********************************************************************
# changed: [localhost -> localhost]
```

WARNING: The following error can occur when recreating a previously deleted terraform backend S3 bucket.

```shell
Failed while creating bucket: An error occurred (OperationAborted) when calling the CreateBucket operation:
A conflicting conditional operation is currently in progress against this resource. Please try again.
```

This is due to AWS which can take up to 1 hour to completely propagate bucket deletion through regions

## Contribute

If you wish to contribute, please follow these guidelines.

### Pre Commit

In order to improve code consistency through this repository,
you will have to install [pre-commit](https://pre-commit.com/#install)
and then run `pre-commit install` to enable pre-commit Git hook to run `pre-commit` automatically at commit time.

You will also be able to run `pre-commit run -a` to trigger the different hooks specified in the [configuration file](./.pre-commit-config.yaml).

You will need to install these tools required by configured hooks:

- [tflint](https://github.com/terraform-linters/tflint#installation)
- [tfsec](https://github.com/tfsec/tfsec#installation)

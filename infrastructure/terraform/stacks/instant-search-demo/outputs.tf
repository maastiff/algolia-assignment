output "cloudfront_distribution_id" {
  description = "The identifier for the CloudFront distribution."
  value       = module.cloudfront_s3_webstatic.cloudfront_distribution_id
}

output "cloudfront_distribution_arn" {
  description = "The ARN (Amazon Resource Name) for the CloudFront distribution."
  value       = module.cloudfront_s3_webstatic.cloudfront_distribution_arn
}

output "cloudfront_distribution_domain_name" {
  description = "The domain name corresponding to the CloudFront distribution."
  value       = var.enable_custom_dns ? var.target_domain_name : module.cloudfront_s3_webstatic.cloudfront_distribution_domain_name
}

output "s3_website_new_version_bucket_arn" {
  description = "The ARN of the bucket hosting the new deployed version of website."
  value       = module.cloudfront_s3_webstatic.s3_website_new_version_bucket_arn
}

output "s3_website_old_version_bucket_arn" {
  description = "The ARN of the bucket hosting the old deployed version of website."
  value       = module.cloudfront_s3_webstatic.s3_website_old_version_bucket_arn
}

output "zzz_message" {
  description = "An informational message to print to terraform user at the end of deployment"
  value       = <<EOF
  The CloudFront distribution has been successfully deployed and can be accessed at:
  https://%{if var.enable_custom_dns}${var.target_domain_name}%{else}${module.cloudfront_s3_webstatic.cloudfront_distribution_domain_name}%{endif}

  You might get an AccessDenied error access if you deployed this stack for the first time. This error occurs when target
  bucket origin is empty; thus CloudFront complains that it cannot return index.html.

  => You should now move to ansible side to deploy instant-search-demo application
  EOF
}

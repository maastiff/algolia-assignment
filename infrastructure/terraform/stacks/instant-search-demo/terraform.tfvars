region = "eu-west-1"

enable_custom_dns  = false
dns_zone_name      = "algolia.brabant.dev"
target_domain_name = "instant-search-demo.algolia.brabant.dev"

website_s3_bucket_name              = "instant-search-demo.algolia"
cloudfront_distribution_description = "The cloudfront distribution for Instant Search Demo application"
cloudfront_access_logs_bucket_name  = "algolia-cloudfront-access-logs"

tags = {
  Application = "instant-search-demo"
  Author      = "Benjamin Brabant"
}

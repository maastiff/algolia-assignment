terraform {
  backend "s3" {
    workspace_key_prefix = "instant-search-demo"
    key                  = "terraform.tfstate"
  }
}

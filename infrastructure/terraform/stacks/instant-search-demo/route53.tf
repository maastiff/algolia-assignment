resource "aws_route53_record" "cloudfront" {
  count = var.enable_custom_dns ? 1 : 0

  zone_id = data.aws_route53_zone.public_hosted_zone[0].id
  name    = var.target_domain_name
  type    = "A"

  alias {
    name                   = module.cloudfront_s3_webstatic.cloudfront_distribution_domain_name
    zone_id                = module.cloudfront_s3_webstatic.cloudfront_distribution_hosted_zone_id
    evaluate_target_health = false
  }
}

module "cloudfront_acm_certificate" {
  count = var.enable_custom_dns ? 1 : 0

  source = "../../modules/acm"

  providers = {
    aws = aws.cloudfront-region
  }
  domain_name = var.target_domain_name

  hosted_zone_id = data.aws_route53_zone.public_hosted_zone[0].zone_id

  tags = var.tags
}

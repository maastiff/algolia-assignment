variable "region" {
  description = "Target AWS region"
  type        = string
}

variable "enable_custom_dns" {
  description = "Whether to enable custom DNS configuration or rely on default CloudFront domain"
  type        = bool
  default     = false
}

variable "dns_zone_name" {
  description = "The target AWS Route53 hosted zone to target for DNS records creation"
  type        = string
}

variable "target_domain_name" {
  description = "The target domain name to target application"
  type        = string
}

variable "website_s3_bucket_name" {
  description = "The S3 bucket name to create to host the static website resources."
  type        = string
}

variable "cloudfront_distribution_description" {
  description = "Any comments you want to include about the CloudFront distribution."
  type        = string
}

variable "cloudfront_access_logs_bucket_name" {
  description = "The S3 bucket name to create to store the cloudfront access logs."
  type        = string
}

variable "tags" {
  description = "A mapping of tags to assign to the resource."
  type        = map(string)
  default     = {}
}

module "cloudfront_s3_webstatic" {
  source = "../../modules/cloudfront-s3-webstatic"
  providers = {
    aws.cloudfront-region = aws.cloudfront-region
  }

  aliases = var.enable_custom_dns ? [var.target_domain_name] : null

  website_s3_bucket_name = var.website_s3_bucket_name
  description            = var.cloudfront_distribution_description

  viewer_certificate = {
    acm_certificate_arn            = var.enable_custom_dns ? module.cloudfront_acm_certificate[0].acm_certificate_arn : null
    cloudfront_default_certificate = var.enable_custom_dns ? null : true
  }

  logging_config = {
    bucket = module.cloudfront_access_logs.s3_bucket_bucket_domain_name
  }

  tags = var.tags
}

module "cloudfront_access_logs" {
  source = "../../modules/s3-access-logs"

  bucket_prefix = var.cloudfront_access_logs_bucket_name
  target_type   = "CLOUDFRONT"
  force_destroy = true

  tags = var.tags
}

data "aws_route53_zone" "public_hosted_zone" {
  count = var.enable_custom_dns ? 1 : 0
  name  = var.dns_zone_name
}

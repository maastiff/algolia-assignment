provider "aws" {
  region = var.region
}

provider "aws" {
  alias  = "cloudfront-region"
  region = "us-east-1"
}

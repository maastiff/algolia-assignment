# CloudFront Rolling Update Lambda@Edge

The lambda cloudfront-rolling-update function aims to rollout traffic gradually
from old origin to new one during a new application version deployment.
This function is deployed as a CloudFront lambda@edge which implies
[several restrictions](https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/edge-functions-restrictions.html)

This function is heavily inspired by this implementation example:
<https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/lambda-examples.html#lambda-examples-content-based-gradual-traffic-transfer>
It expects to be triggered by an `origin-request` CloudFront event in order to being able to switch the target origin.

## Setup

### Local

This project required Python 3.8 that you can manage through [pyenv](https://github.com/pyenv/pyenv).

This project rely on [pipenv](./Pipfile) to manage dependencies
and underlying [Python virtualenv](https://docs.python-guide.org/dev/virtualenvs/) for us.
To install required dependencies:

```shell
pipenv install --dev
```

Note: This function depends on [boto3](https://pypi.org/project/boto3/) and [botocore](https://pypi.org/project/botocore/)
which are already bundled in lambda Python runtime.
<https://docs.aws.amazon.com/lambda/latest/dg/lambda-runtimes.html>

To activate Python virtualenv and rely on project dependencies:

```shell
pipenv shell
```

Or:

```shell
source $(pipenv --venv)/bin/activate
```

### Docker

This project rely on Docker container lambda package to execute the function locally
<https://docs.aws.amazon.com/lambda/latest/dg/python-image.html>

You would need to build the Docker image this way:

```shell
make build
# or
docker-compose build
```

Then you can run the container:

```shell
make run
# or
docker-compose up -d
```

This will create a container which expose an API on `9999` port to interact with the lambda.
You can then POST an [event](./examples/cloudfront-origin-request-event.json) to trigger this lambda:

```shell
curl -XPOST "http://localhost:9999/2015-03-31/functions/function/invocations" --data-binary "@examples/cloudfront-origin-request-event.json"
```

## Usage

You can run this lambda function locally, packaged as a Docker container, to perform CloudFront origin request transformation.
You can also run this lambda function directly from your local Python environment without Docker setup,
leveraging [main wrapper](./cloudfront_rolling_update/__main__.py).

### Local

You can manually trigger the cloudfront-rolling-update lambda function presented previously,
directly from your local Python environment:

```shell
pipenv run local ./examples/cloudfront-origin-request-event.json
```

### Docker

You can trigger cloudfront-rolling-update lambda the following way:

```shell
make trigger
# or
curl -XPOST "http://localhost:9999/2015-03-31/functions/function/invocations" --data-binary "@examples/cloudfront-origin-request-event.json"
```

which should result in the following response (please notice the `host` header value):

```json
{
  "clientIp": "91.168.5.236",
  "headers": {
    "host": [
      {
        "key": "host",
        "value": "old.instant-search-demo.algolia.brabant.s3.eu-west-1.amazonaws.com"
      }
    ],
    "x-forwarded-for": [
      {
        "key": "X-Forwarded-For",
        "value": "91.168.5.236"
      }
    ],
    "user-agent": [
      {
        "key": "User-Agent",
        "value": "Amazon CloudFront"
      }
    ],
    "via": [
      {
        "key": "Via",
        "value": "2.0 8c00584bf409a3f42ec7f0aef27ef265.cloudfront.net (CloudFront)"
      }
    ],
    "accept-encoding": [
      {
        "key": "Accept-Encoding",
        "value": "gzip"
      }
    ],
    "pragma": [
      {
        "key": "Pragma",
        "value": "no-cache"
      }
    ],
    "sec-ch-ua": [
      {
        "key": "sec-ch-ua",
        "value": "\" Not;A Brand\";v=\"99\", \"Google Chrome\";v=\"91\", \"Chromium\";v=\"91\""
      }
    ],
    "sec-ch-ua-mobile": [
      {
        "key": "sec-ch-ua-mobile",
        "value": "?0"
      }
    ],
    "dnt": [
      {
        "key": "dnt",
        "value": "1"
      }
    ],
    "upgrade-insecure-requests": [
      {
        "key": "upgrade-insecure-requests",
        "value": "1"
      }
    ],
    "sec-fetch-site": [
      {
        "key": "sec-fetch-site",
        "value": "none"
      }
    ],
    "sec-fetch-mode": [
      {
        "key": "sec-fetch-mode",
        "value": "navigate"
      }
    ],
    "sec-fetch-user": [
      {
        "key": "sec-fetch-user",
        "value": "?1"
      }
    ],
    "sec-fetch-dest": [
      {
        "key": "sec-fetch-dest",
        "value": "document"
      }
    ],
    "cache-control": [
      {
        "key": "Cache-Control",
        "value": "no-cache"
      }
    ]
  },
  "method": "GET",
  "origin": {
    "s3": {
      "authMethod": "origin-access-identity",
      "customHeaders": {},
      "domainName": "old.instant-search-demo.algolia.brabant.s3.eu-west-1.amazonaws.com",
      "path": "",
      "region": "eu-west-1"
    }
  },
  "querystring": "",
  "uri": "/index.html"
}
```

## Contribute

If you wish to contribute, please follow these guidelines.

### Pre Commit

In order to improve code consistency through this repository, you will have to install [pre-commit](https://pre-commit.com/#install)
and then run `pre-commit install` in order to enable pre-commit Git hook to run `pre-commit` automatically at commit time.

You will also be able to run `pre-commit run -a`
in order to trigger the different hooks specified in the [configuration file](./.pre-commit-config.yaml).

### Tests

In order to ensure stability through evolution, you should run and write tests to cover change made in the codebase.
You can run tests the following way:

```shell
make tests
# or
pipenv run pytest

========================================================================= test session starts =========================================================================
platform darwin -- Python 3.8.10, pytest-6.2.4, py-1.10.0, pluggy-0.13.1 -- ...
cachedir: .pytest_cache
rootdir: .../algolia-assignment/infrastructure/terraform/modules/cloudfront-s3-webstatic/lambda, configfile: pyproject.toml, testpaths: tests
plugins: datadir-1.3.1
collected 1 item

tests/lambda_function_i_test.py::test_lambda_handler_should_return_expected_transformed_origin_request PASSED                                                   [100%]

==============
```

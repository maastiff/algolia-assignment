"""
This main wrapper aims to provide a way to trigger lambda function locally without requiring an AWS lambda runtime
"""

import argparse
import json
import os

from cloudfront_rolling_update.lambda_function import lambda_handler


class OpenFileAction(argparse.Action):  # pylint: disable=too-few-public-methods
    """OpenFileAction class represent an argparse Action which automatically open provided file"""

    def __init__(self, option_strings, dest, nargs=None, **kwargs):
        super().__init__(option_strings, dest, **kwargs)

    def __call__(self, parser, namespace, values, option_string=None):
        with open(os.path.abspath(values), mode="r") as file:
            setattr(namespace, self.dest, json.loads(file.read()))


def main():
    """The main entrypoint which expect an event json file as command line parameter to call lambda_handler function"""
    parser = argparse.ArgumentParser(
        description="Trigger cloudfront_rolling_update lambda."
    )
    parser.add_argument(
        "event",
        action=OpenFileAction,
        help="The event to provide while triggering lambda function",
    )

    args = parser.parse_args()
    lambda_handler(args.event, None)


if __name__ == "__main__":
    main()

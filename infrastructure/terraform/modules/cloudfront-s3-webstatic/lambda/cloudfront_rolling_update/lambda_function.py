"""
CloudFront Rolling Update lambda handler.
This function aims to rollout traffic gradually from old origin to new one during a new application version deployment.
This function is deployed as a CloudFront lambda@edge which implies several restrictions.
"""

import logging
import os

import boto3

logger = logging.getLogger()
logger.setLevel(logging.INFO)

COOKIE_FORCE_TARGET_ORIGIN = "X-CloudFront-Force-Current-Target"
SSM_ENDPOINT_URL = os.environ.get("SSM_ENDPOINT_URL")


def lambda_handler(event, context):
    """
    This function is triggered by AWS Lambda in order to switch between CloudFront origins during application update.

        Parameters:
            event (json): The AWS Lambda event representing CloudFront origin-request event
            context (json): The AWS Lambda context

        Returns
            request (json): The transformed CloudFront origin-request which target expected origin
    """

    logger.info("## ENVIRONMENT VARIABLES")
    logger.info(os.environ)
    logger.info("## EVENT")
    logger.info(event)
    logger.info("## CONTEXT")
    logger.info(context)

    ssm = boto3.client("ssm", region_name="us-east-1", endpoint_url=SSM_ENDPOINT_URL)

    request = event["Records"][0]["cf"]["request"]
    parsed_cookies = _parsed_cookies(request["headers"])

    current_target_origin = (
        parsed_cookies[COOKIE_FORCE_TARGET_ORIGIN.lower()]
        if parsed_cookies and parsed_cookies[COOKIE_FORCE_TARGET_ORIGIN.lower()]
        else _get_ssm_parameter(ssm, "/cloudfront-rolling-update/origin/current-target")
    )

    current_target_domain_name_parameter_path = (
        f"/cloudfront-rolling-update/origin/{current_target_origin}-target-domain-name"
    )
    current_target_domain_name = _get_ssm_parameter(
        ssm, current_target_domain_name_parameter_path
    )

    request["origin"]["s3"]["domainName"] = current_target_domain_name
    request["headers"]["host"] = [{"key": "host", "value": current_target_domain_name}]

    return request


def _get_ssm_parameter(ssm, parameter_name):
    parameter = ssm.get_parameter(Name=parameter_name)
    parameter_value = parameter["Parameter"]["Value"]
    logging.info("SSM PARAMETER (%s): %s", parameter_name, parameter_value)
    return parameter_value


def _parsed_cookies(headers):
    parsed_cookies = {}
    if headers.get("cookie"):
        for cookie in headers["cookie"][0]["value"].split(";"):
            if cookie:
                parts = cookie.split("=")
                parsed_cookies[parts[0].strip().lower()] = parts[1].strip()
    return parsed_cookies

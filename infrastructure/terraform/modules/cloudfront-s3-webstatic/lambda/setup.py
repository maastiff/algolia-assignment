#!/usr/bin/env python
from setuptools import setup, find_packages

setup(
    name="cloudfront_rolling_update",
    version="1.0",
    packages=find_packages(exclude=["tests", "tests.*"]),
    license="",
    long_description=open("README.md").read(),
    include_package_data=True,
    classifiers={"Programming Language :: Python :: 3"},
    entry_points={
        "console_scripts": [
            "cloudfront_rolling_update = cloudfront_rolling_update.__main__:main",
        ]
    },
)

import json

from pytest import fixture

from cloudfront_rolling_update.lambda_function import lambda_handler


@fixture(autouse=True)
def before_each(ssm):
    ssm.put_parameter(
        Name="/cloudfront-rolling-update/origin/current-target",
        Description="The CloudFront current target origin",
        Value="old",
        Type="String",
        Overwrite=True,
        AllowedPattern="old|new",
    )
    ssm.put_parameter(
        Name="/cloudfront-rolling-update/origin/new-target-domain-name",
        Description="The CloudFront new target origin domain name",
        Value="new.instant-search-demo.algolia.brabant.s3.eu-west-1.amazonaws.com",
        Type="String",
        Overwrite=True,
    )
    ssm.put_parameter(
        Name="/cloudfront-rolling-update/origin/old-target-domain-name",
        Description="The CloudFront old target origin domain name",
        Value="old.instant-search-demo.algolia.brabant.s3.eu-west-1.amazonaws.com",
        Type="String",
        Overwrite=True,
    )


def test_lambda_handler_should_return_expected_transformed_origin_request(datadir, ssm):
    # Given
    cloudfront_origin_request_event = json.loads(
        (datadir / "cloudfront-origin-request-event.json").read_text()
    )

    # When
    transformed_origin_request = lambda_handler(cloudfront_origin_request_event, None)

    # Then
    cloudfront_expected_transformed_origin_request = json.loads(
        (
            datadir
            / "cloudfront-origin-request-event-expected-transformed-request.json"
        ).read_text()
    )
    assert transformed_origin_request == cloudfront_expected_transformed_origin_request


def test_lambda_handler_should_return_new_target_origin_when_overriding_current_target_with_cookie(
    datadir, ssm
):
    # Given
    cloudfront_origin_request_event = json.loads(
        (
            datadir / "cloudfront-origin-request-event-with-cookie-override.json"
        ).read_text()
    )

    # When
    transformed_origin_request = lambda_handler(cloudfront_origin_request_event, None)

    # Then
    expected_target_origin = (
        "new.instant-search-demo.algolia.brabant.s3.eu-west-1.amazonaws.com"
    )
    assert (
        transformed_origin_request["headers"]["host"][0]["value"]
        == expected_target_origin
    )
    assert (
        transformed_origin_request["origin"]["s3"]["domainName"]
        == expected_target_origin
    )

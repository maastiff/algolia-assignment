echo "Creating AWS SSM parameter '/cloudfront-rolling-update/origin/current-target' with target '$CLOUDFRONT_ROLLING_UPDATE_ORIGIN_DEFAULT_TARGET' value by default..."
awslocal ssm put-parameter \
    --name "/cloudfront-rolling-update/origin/current-target" \
    --description "The CloudFront current target origin" \
    --type "String" \
    --value "$CLOUDFRONT_ROLLING_UPDATE_ORIGIN_DEFAULT_TARGET" \
    --overwrite \
    --allowed-pattern "old|new"
echo "... Finished"

echo "Creating AWS SSM parameter '/cloudfront-rolling-update/origin/new-target-domain-name' defined to '$CLOUDFRONT_ROLLING_UPDATE_NEW_TARGET_DOMAIN_NAME'..."
awslocal ssm put-parameter \
    --name "/cloudfront-rolling-update/origin/new-target-domain-name" \
    --description "The CloudFront new target origin domain name" \
    --type "String" \
    --value "$CLOUDFRONT_ROLLING_UPDATE_NEW_TARGET_DOMAIN_NAME" \
    --overwrite
echo "... Finished"

echo "Creating AWS SSM parameter '/cloudfront-rolling-update/origin/old-target-domain-name' defined to '$CLOUDFRONT_ROLLING_UPDATE_OLD_TARGET_DOMAIN_NAME'..."
awslocal ssm put-parameter \
    --name "/cloudfront-rolling-update/origin/old-target-domain-name" \
    --description "The CloudFront old target origin domain name" \
    --type "String" \
    --value "$CLOUDFRONT_ROLLING_UPDATE_OLD_TARGET_DOMAIN_NAME" \
    --overwrite
echo "... Finished"

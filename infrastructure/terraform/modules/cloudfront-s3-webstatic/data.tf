data "aws_caller_identity" "current" {}

data "aws_cloudfront_cache_policy" "this" {
  name = local.default_cache_behavior["cache_policy_name"]
}

data "aws_cloudfront_origin_request_policy" "this" {
  count = lookup(local.default_cache_behavior, "origin_request_policy_name", "") != "" ? 1 : 0
  name  = local.default_cache_behavior["origin_request_policy_name"]
}

data "archive_file" "cloudfront_rolling_update" {
  type        = "zip"
  source_file = "${path.module}/lambda/cloudfront_rolling_update/lambda_function.py"
  output_path = "${path.module}/artifacts/cloudfront-rolling-update-lambda.zip"
}

data "aws_iam_policy_document" "cloudfront_rolling_update_lambda_logs_policy" {
  version = "2012-10-17"

  statement {
    effect = "Allow"
    actions = [
      "logs:CreateLogStream",
      "logs:DescribeLogStreams",
      "logs:CreateLogGroup",
    ]

    resources = [
      "arn:aws:logs:*:${data.aws_caller_identity.current.account_id}:log-group:/aws/lambda/us-east-1.${local.cloudfront_rolling_update_lambda_function_name}:*",
    ]
  }

  statement {
    effect = "Allow"
    actions = [
      "logs:PutLogEvents",
    ]

    resources = [
      "arn:aws:logs:*:${data.aws_caller_identity.current.account_id}:log-group:/aws/lambda/us-east-1.${local.cloudfront_rolling_update_lambda_function_name}:*",
    ]
  }
}

data "aws_iam_policy_document" "cloudfront_rolling_update_lambda_ssm_policy" {
  version = "2012-10-17"

  statement {
    effect = "Allow"
    actions = [
      "ssm:GetParameter",
    ]

    resources = [
      "*",
    ]
  }
}

data "aws_iam_policy_document" "cloudfront_rolling_update_lambda_execution_role_policy" {
  source_policy_documents = [
    data.aws_iam_policy_document.cloudfront_rolling_update_lambda_logs_policy.json,
    data.aws_iam_policy_document.cloudfront_rolling_update_lambda_ssm_policy.json
  ]
}

data "aws_iam_policy_document" "cloudfront_rolling_update_assume_role_policy" {
  version = "2012-10-17"

  statement {
    sid    = ""
    effect = "Allow"

    actions = ["sts:AssumeRole"]

    principals {
      type = "Service"

      identifiers = [
        "lambda.amazonaws.com",
        "edgelambda.amazonaws.com",
      ]
    }
  }
}

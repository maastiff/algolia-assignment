module "new_version_s3_origin" {
  source = "./modules/s3-origin"

  website_s3_bucket_prefix                  = "new.${var.website_s3_bucket_name}"
  cloudfront_origin_access_identity_iam_arn = aws_cloudfront_origin_access_identity.this.iam_arn
}

module "old_version_s3_origin" {
  source = "./modules/s3-origin"

  website_s3_bucket_prefix                  = "old.${var.website_s3_bucket_name}"
  cloudfront_origin_access_identity_iam_arn = aws_cloudfront_origin_access_identity.this.iam_arn
}

locals {
  cloudfront_rolling_update_lambda_function_name = "lambda-rolling-update-origin"

  cache_policy_id          = data.aws_cloudfront_cache_policy.this.id
  origin_request_policy_id = lookup(local.default_cache_behavior, "origin_request_policy_name", null) != null ? data.aws_cloudfront_origin_request_policy.this[0].id : aws_cloudfront_origin_request_policy.this.id

  default_cache_behavior = defaults(var.default_cache_behavior, {
    compress               = false
    cache_policy_name      = "Managed-CachingDisabled"
    viewer_protocol_policy = "redirect-to-https"
  })

  cloudfront_default_certificate = var.viewer_certificate.acm_certificate_arn == null && var.viewer_certificate.iam_certificate_id == null
  viewer_certificate = defaults(var.viewer_certificate, {
    cloudfront_default_certificate = local.cloudfront_default_certificate ? true : null
    minimum_protocol_version       = coalesce(var.viewer_certificate.cloudfront_default_certificate, local.cloudfront_default_certificate) ? "TLSv1" : "TLSv1.2_2021"
    ssl_support_method             = coalesce(var.viewer_certificate.cloudfront_default_certificate, local.cloudfront_default_certificate) ? null : "sni-only"
  })
}

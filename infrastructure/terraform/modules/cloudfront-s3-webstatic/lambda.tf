resource "aws_lambda_function" "cloudfront_rolling_update" {
  provider = aws.cloudfront-region

  function_name = local.cloudfront_rolling_update_lambda_function_name
  description   = <<EOF
    CloudFront Lambda@Edge to gradually move trafic from old version origin to new one when deploying application
    EOF
  filename      = abspath("${path.module}/artifacts/cloudfront-rolling-update-lambda.zip")
  role          = aws_iam_role.cloudfront_rolling_update_execution_role.arn
  handler       = "lambda_function.lambda_handler"

  source_code_hash = data.archive_file.cloudfront_rolling_update.output_base64sha256

  runtime = "python3.8"
  timeout = 5
  publish = true

  tags = var.tags
}

resource "aws_iam_role" "cloudfront_rolling_update_execution_role" {
  provider = aws.cloudfront-region

  name               = "role-cloudfront-rolling-update-lambda"
  assume_role_policy = data.aws_iam_policy_document.cloudfront_rolling_update_assume_role_policy.json

  inline_policy {
    name   = "cloudfront_rolling_update_policy"
    policy = data.aws_iam_policy_document.cloudfront_rolling_update_lambda_execution_role_policy.json
  }

  tags = var.tags
}


resource "aws_ssm_parameter" "cloudfront_rolling_update_origin_current_target" {
  provider = aws.cloudfront-region

  name            = "/cloudfront-rolling-update/origin/current-target"
  description     = "The CloudFront current target origin"
  type            = "String"
  value           = "old"
  overwrite       = true
  allowed_pattern = "old|new"
}

resource "aws_ssm_parameter" "cloudfront_rolling_update_new_target_domain_name" {
  provider = aws.cloudfront-region

  name        = "/cloudfront-rolling-update/origin/new-target-domain-name"
  description = "The CloudFront new target origin domain name"
  type        = "String"
  value       = module.new_version_s3_origin.bucket_regional_domain_name
  overwrite   = true
}

resource "aws_ssm_parameter" "cloudfront_rolling_update_old_target_domain_name" {
  provider = aws.cloudfront-region

  name        = "/cloudfront-rolling-update/origin/old-target-domain-name"
  description = "The CloudFront old target origin domain name"
  type        = "String"
  value       = module.old_version_s3_origin.bucket_regional_domain_name
  overwrite   = true
}

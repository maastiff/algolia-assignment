# tfsec:ignore:AWS021 tfsec:ignore:AWS002 tfsec:ignore:AWS077 tfsec:ignore:AWS098
resource "aws_s3_bucket" "this" {
  bucket        = var.website_s3_bucket_name
  bucket_prefix = var.website_s3_bucket_prefix

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  force_destroy = true
  tags          = var.tags
}

resource "aws_s3_bucket_public_access_block" "policy" {
  bucket = aws_s3_bucket_policy.bucket_policy.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

data "aws_iam_policy_document" "s3_policy" {
  statement {
    actions   = ["s3:GetObject"]
    resources = [format("%s/*", aws_s3_bucket.this.arn)]

    principals {
      type        = "AWS"
      identifiers = [var.cloudfront_origin_access_identity_iam_arn]
    }
  }
}

resource "aws_s3_bucket_policy" "bucket_policy" {
  bucket = aws_s3_bucket.this.id

  policy = data.aws_iam_policy_document.s3_policy.json
}

variable "website_s3_bucket_name" {
  description = <<EOF
      (Optional, Forces new resource) The name of the bucket to create to host the static website resources.
    If omitted, Terraform will assign a random, unique name.
    EOF
  type        = string
  default     = null
}

variable "website_s3_bucket_prefix" {
  description = <<EOF
      (Optional, Forces new resource) Creates a unique bucket name to host the static website resources
        beginning with the specified prefix.
      Conflicts with bucket.
      EOF
  type        = string
  default     = null
}

variable "cloudfront_origin_access_identity_iam_arn" {
  description = "The IAM arn of the CloudFront origin access identity created to associate with this bucket"
  type        = string
}

variable "tags" {
  description = "A map of tags to assign to the resource."
  type        = map(string)
  default     = {}
}

resource "aws_cloudfront_origin_access_identity" "this" {}

# tfsec:ignore:AWS021 tfsec:ignore:AWS045 tfsec:ignore:AWS071
resource "aws_cloudfront_distribution" "this" {
  enabled         = var.enabled
  price_class     = var.price_class
  is_ipv6_enabled = var.is_ipv6_enabled
  comment         = var.description
  aliases         = var.aliases

  tags = var.tags

  origin {
    domain_name = module.new_version_s3_origin.bucket_regional_domain_name
    origin_id   = module.new_version_s3_origin.bucket_regional_domain_name

    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.this.cloudfront_access_identity_path
    }
  }

  origin {
    domain_name = module.old_version_s3_origin.bucket_regional_domain_name
    origin_id   = module.old_version_s3_origin.bucket_regional_domain_name

    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.this.cloudfront_access_identity_path
    }
  }

  default_root_object = var.default_root_object

  default_cache_behavior {
    allowed_methods = local.default_cache_behavior["allowed_methods"]
    cached_methods  = local.default_cache_behavior["cached_methods"]
    compress        = local.default_cache_behavior["compress"]

    cache_policy_id          = local.cache_policy_id
    origin_request_policy_id = local.origin_request_policy_id

    target_origin_id = module.new_version_s3_origin.bucket_regional_domain_name

    viewer_protocol_policy = local.default_cache_behavior["viewer_protocol_policy"]

    lambda_function_association {
      event_type   = "origin-request"
      lambda_arn   = aws_lambda_function.cloudfront_rolling_update.qualified_arn
      include_body = false
    }
  }

  restrictions {
    geo_restriction {
      restriction_type = var.geo_restriction.restriction_type
      locations        = var.geo_restriction.locations
    }
  }

  viewer_certificate {
    acm_certificate_arn            = local.viewer_certificate["acm_certificate_arn"]
    cloudfront_default_certificate = local.viewer_certificate["cloudfront_default_certificate"]
    iam_certificate_id             = local.viewer_certificate["iam_certificate_id"]

    minimum_protocol_version = local.viewer_certificate["minimum_protocol_version"]
    ssl_support_method       = local.viewer_certificate["ssl_support_method"]
  }

  dynamic "logging_config" {
    for_each = var.logging_config == null ? [] : [var.logging_config]

    content {
      bucket          = logging_config.value["bucket"]
      prefix          = lookup(logging_config.value, "prefix", null)
      include_cookies = lookup(logging_config.value, "include_cookies", null)
    }
  }
}

resource "aws_cloudfront_origin_request_policy" "this" {
  name    = "AllowCloudFrontRollingUpdateCookie"
  comment = "Allow ${var.cloudfront_force_target_origin_cookie_name} cookie to be forwarded to origin"

  cookies_config {
    cookie_behavior = "whitelist"
    cookies {
      items = [var.cloudfront_force_target_origin_cookie_name]
    }
  }
  headers_config {
    header_behavior = "whitelist"
    headers {
      items = ["origin", "access-control-request-headers", "access-control-request-method"]
    }
  }
  query_strings_config {
    query_string_behavior = "none"
  }
}

variable "description" {
  description = "Any comments you want to include about the distribution."
  type        = string
}

variable "aliases" {
  description = "Extra CNAMEs (alternate domain names), if any, for this distribution."
  type        = set(string)
  default     = null
}

variable "enabled" {
  description = "Whether the distribution is enabled to accept end user requests for content."
  type        = bool
  default     = true
}

variable "website_s3_bucket_name" {
  description = "The S3 bucket name to create to host the static website resources."
  type        = string
}

variable "default_root_object" {
  description = "The object that you want CloudFront to return (for example, index.html) when an end user requests the root URL."
  type        = string
  default     = "index.html"
}

variable "is_ipv6_enabled" {
  description = "Whether the IPv6 is enabled for the distribution."
  type        = bool
  default     = null
}

variable "price_class" {
  description = "The price class for this distribution. One of PriceClass_All, PriceClass_200, PriceClass_100"
  type        = string
  default     = null
}

variable "default_cache_behavior" {
  description = "The default cache behavior for this distribution"
  type = object({
    allowed_methods = set(string)
    cached_methods  = set(string)
    compress        = optional(bool)

    cache_policy_name          = optional(string)
    origin_request_policy_name = optional(string)

    viewer_protocol_policy = optional(string)
  })
  default = {
    allowed_methods = ["HEAD", "GET"]
    cached_methods  = ["HEAD", "GET"]
    compress        = false

    cache_policy_name = "Managed-CachingDisabled"

    viewer_protocol_policy = "redirect-to-https"
  }
}

variable "geo_restriction" {
  description = "The restriction configuration for this distribution (geo_restrictions)"
  type = object({
    restriction_type = string
    locations        = set(string)
  })
  default = {
    restriction_type = "none"
    locations        = []
  }
}

variable "viewer_certificate" {
  description = "The SSL configuration for this distribution"
  type = object({
    acm_certificate_arn            = optional(string)
    cloudfront_default_certificate = optional(bool)
    iam_certificate_id             = optional(string)

    minimum_protocol_version = optional(string)
    ssl_support_method       = optional(string)
  })
  default = {
    cloudfront_default_certificate = true
  }
}

variable "logging_config" {
  description = "The logging configuration that controls how logs are written to your distribution (maximum one)."
  type = object({
    bucket          = string
    include_cookies = optional(bool)
    prefix          = optional(string)
  })
  default = null
}

variable "cloudfront_force_target_origin_cookie_name" {
  description = "Cookie name which is used to force switching between old or new CloudFront target origin"
  type        = string
  default     = "X-CloudFront-Force-Current-Target"
}

variable "tags" {
  description = "A map of tags to assign to the resource."
  type        = map(string)
  default     = {}
}

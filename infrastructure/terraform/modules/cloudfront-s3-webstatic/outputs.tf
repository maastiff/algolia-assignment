output "cloudfront_distribution_id" {
  description = "The identifier for the distribution."
  value       = aws_cloudfront_distribution.this.id
}

output "cloudfront_distribution_arn" {
  description = "The ARN (Amazon Resource Name) for the distribution."
  value       = aws_cloudfront_distribution.this.arn
}

output "cloudfront_distribution_hosted_zone_id" {
  description = "The CloudFront Route 53 zone ID that can be used to route an Alias Resource Record Set to."
  value       = aws_cloudfront_distribution.this.hosted_zone_id
}

output "cloudfront_distribution_domain_name" {
  description = "The domain name corresponding to the distribution."
  value       = aws_cloudfront_distribution.this.domain_name
}

output "cloudfront_origin_access_identity" {
  description = "The origin access identity created"
  value       = aws_cloudfront_origin_access_identity.this
}

output "cloudfront_origin_access_identity_id" {
  description = "The ID of the origin access identity created"
  value       = aws_cloudfront_origin_access_identity.this.id
}

output "cloudfront_origin_access_identity_iam_arn" {
  description = "The IAM arn of the origin access identity created"
  value       = aws_cloudfront_origin_access_identity.this.iam_arn
}

output "s3_website_new_version_bucket_id" {
  description = "The name of the bucket hosting the new deployed version of website."
  value       = module.new_version_s3_origin.bucket_id
}

output "s3_website_new_version_bucket_arn" {
  description = "The ARN of the bucket hosting the new deployed version of website."
  value       = module.new_version_s3_origin.bucket_arn
}

output "s3_website_old_version_bucket_id" {
  description = "The name of the bucket hosting the old deployed version of website."
  value       = module.old_version_s3_origin.bucket_id
}

output "s3_website_old_version_bucket_arn" {
  description = "The ARN of the bucket hosting the old deployed version of website."
  value       = module.old_version_s3_origin.bucket_arn
}

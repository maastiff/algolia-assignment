output "acm_certificate_arn" {
  description = "The ARN of the certificates"
  value       = element(concat(aws_acm_certificate_validation.this[*].certificate_arn, aws_acm_certificate.this[*].arn, [""]), 0)
}

output "acm_certificate_domain_validation_options" {
  description = <<EOF
      A list of attributes to feed into other resources to complete certificate validation.
      Can have more than one element, e.g. if SANs are defined."
      EOF
  value       = aws_acm_certificate.this.domain_validation_options
}

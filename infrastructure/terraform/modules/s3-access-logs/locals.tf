locals {
  grants = {
    CLOUDFRONT = [
      {
        type        = "CanonicalUser"
        permissions = ["FULL_CONTROL"]
        id          = data.aws_canonical_user_id.current.id
      },
      {
        type        = "CanonicalUser"
        permissions = ["FULL_CONTROL"]
        id          = "c4c1ede66af53448b93c283ce9448c4ba468c9432aa01d700d3878632f77d2d0"
    }]

    S3 = [
      {
        type        = "Group"
        permissions = ["READ_ACP", "WRITE"]
        uri         = "http://acs.amazonaws.com/groups/s3/LogDelivery"
    }]
  }
}

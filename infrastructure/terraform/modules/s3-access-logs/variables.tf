variable "bucket" {
  description = <<EOF
    (Optional, Forces new resource) The name of the bucket.
    If omitted, Terraform will assign a random, unique name.
    EOF
  type        = string
  default     = null
}

variable "bucket_prefix" {
  description = <<EOF
      (Optional, Forces new resource) Creates a unique bucket name beginning with the specified prefix.
      Conflicts with bucket.
      EOF
  type        = string
  default     = null
}

variable "target_type" {
  description = "Target service between LOAD_BALANCER, S3, CLOUDFRONT this bucket will be used for access logging"
  type        = string

  validation {
    condition     = contains(["LOAD_BALANCER", "S3", "CLOUDFRONT"], var.target_type)
    error_message = "Allowed values for target_type are \"LOAD_BALANCER\", \"S3\", or \"CLOUDFRONT\"."
  }
}

variable "lifecycle_standard_ia_transition_days" {
  description = "Number of days before current object versions transition to STANDARD_IA class."
  type        = number
  default     = 60
}

variable "lifecycle_glacier_transition_days" {
  description = "Number of days before current object versions is archived to GLACIER storage."
  type        = number
  default     = 90
}

variable "lifecycle_standard_ia_noncurrent_version_transition_days" {
  description = "Number of days before noncurrent object versions transition to STANDARD_IA class."
  type        = number
  default     = 30
}

variable "lifecycle_glacier_noncurrent_version_transition_days" {
  description = "Number of days before noncurrent object versions is archived to GLACIER storage."
  type        = number
  default     = 60
}

variable "force_destroy" {
  description = <<EOF
    A boolean that indicates all objects (including any locked objects) should be deleted from the bucket
    so that the bucket can be destroyed without error. These objects are not recoverable.
    EOF
  type        = bool
  default     = false
}

variable "tags" {
  description = "A map of tags to assign to the resource."
  type        = map(string)
  default     = {}
}

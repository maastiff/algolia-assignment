region         = "eu-west-1"
bucket         = "terraform-algolia-brabant"
dynamodb_table = "terraform-algolia-brabant"
encrypt        = true

force_path_style = true

skip_credentials_validation = true
skip_metadata_api_check = true
skip_region_validation = true

endpoint = "http://localhost:4566"
dynamodb_endpoint = "http://localhost:4566"
iam_endpoint = "http://localhost:4566"
sts_endpoint = "http://localhost:4566"

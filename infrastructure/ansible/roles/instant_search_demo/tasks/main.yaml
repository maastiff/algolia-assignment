---

- name: Retrieve CloudFront distribution URL
  block:
    - name: Retrieve all AWS CloudFront distributions informations from AWS account
      community.aws.cloudfront_info:
        summary: true
      register: instant_search_demo_cloudfront_info

    - name: Filter AWS CloudFront instant_search_demo distribution by tags
      set_fact:
        instant_search_demo_cloudfront_distribution: "{{
          instant_search_demo_cloudfront_info.cloudfront.summary.distributions
            | selectattr(\"Tags.Application\", \"defined\")
            | selectattr(\"Tags.Application\", \"search\", \"instant-search-demo\")
            | sort(attribute='LastModifiedTime')
            | last }}"

    - name: Retrieve instant_search_demo CloudFront distribution domain name and aliases
      set_fact:
        instant_search_demo_cloudfront_domain_name: "{{ instant_search_demo_cloudfront_distribution.DomainName }}"
        instant_search_demo_cloudfront_alias: "{{ instant_search_demo_cloudfront_distribution.Aliases }}"

    - name: Retrieve instant_search_demo CloudFront distribution URL
      set_fact:
        instant_search_demo_cloudfront_url: "{{
          (instant_search_demo_cloudfront_alias | length > 0)
             | ternary ((instant_search_demo_cloudfront_alias | first), instant_search_demo_cloudfront_domain_name) }}"
  when: not local

- name: Retrieve S3 origin bucket names
  block:
    - name: Retrieve all AWS S3 buckets informations from AWS account
      community.aws.aws_s3_bucket_info:
        endpoint_url: "{{ aws_endpoint_url | default(omit) }}"
      register: instant_search_demo_bucket_info

    - name: Retrieve instant_search_demo old and new target bucket name
      set_fact:
        instant_search_demo_old_target_bucket: "{{
          instant_search_demo_bucket_info.buckets
            | selectattr(\"name\", \"search\", instant_search_demo_old_target_bucket_prefix)
            | sort(attribute='creation_date')
            | map(attribute='name') | last }}"
        instant_search_demo_new_target_bucket: "{{
          instant_search_demo_bucket_info.buckets
            | selectattr(\"name\", \"search\", instant_search_demo_new_target_bucket_prefix)
            | sort(attribute='creation_date')
            | map(attribute='name') | last }}"


- name: Begin deployment of Instant Search Demo new application version
  block:
    - name: Ensure that CloudFront already forward traffic to old target origin
      local_action:
        module: community.aws.aws_ssm_parameter_store
        endpoint_url: "{{ aws_endpoint_url | default(omit) }}"
        region: "{{ aws_cloudfront_region }}"
        name: "{{ ssm_current_target_parameter }}"
        value: "old"

    - name: Create environment file from template into artifact directory
      local_action:
        module: ansible.builtin.template
        src: "templates/env.js.j2"
        dest: "{{ instant_search_demo_local_artefact_path }}/env.js"
        mode: "u=rw,g=r,o=r"

    - name: Deploy Instant Search Demo application to S3 Bucket '{{ instant_search_demo_new_target_bucket }}'
      local_action:
        module: community.aws.s3_sync
        endpoint_url: "{{ aws_endpoint_url | default(omit) }}"
        region: "{{ aws_region }}"
        bucket: "{{ instant_search_demo_new_target_bucket }}"
        file_root: "{{ instant_search_demo_local_artefact_path }}"
        file_change_strategy: "{{ instant_search_demo_force | ternary('force', 'date_size') }}"
        delete: true
      register: instant_search_demo_s3_sync_result

    - name: No change in new application version, end here.
      meta: end_play
      when: not instant_search_demo_s3_sync_result is changed and not instant_search_demo_force

    - local_action:  # noqa unnamed-task
        module: ansible.builtin.pause
        prompt: |
          Ensure that new deployed version is working correctly.
          You can set '{{ cookie_force_target_origin }}=new' cookie in browser to force CloudFront target new origin

          You can also leverage cypress acceptance tests defined in instant-search-demo source directory:

              CYPRESS_BASE_URL="https://{{ instant_search_demo_cloudfront_url }}" \
              CYPRESS_SEARCH_BAR_COLOR="DEFAULT | BLUE | GREEN | RED" \
              CYPRESS_FORCE_CLOUDFRONT_ORIGIN="new" \
              npx cypress run --no-exit --headed --spec "cypress/integration/instant_search_demo_spec.js"

          Do you want to continue? (yes/no)
      register: continue_rolling_update

    - local_action:  # noqa unnamed-task
        module: ansible.builtin.assert
        that:
          - continue_rolling_update.user_input | bool
        success_msg: "Rolling update continue after user validation"
        fail_msg: "Rolling update stop here since problem has been encountered with new application version"


- name: Finish deployment of Instant Search Demo new application version
  block:
    - name: Configure CloudFront Parameter to forward traffic to new target origin
      local_action:
        module: community.aws.aws_ssm_parameter_store
        endpoint_url: "{{ aws_endpoint_url | default(omit) }}"
        region: "{{ aws_cloudfront_region }}"
        name: "{{ ssm_current_target_parameter }}"
        value: "new"

    - name: Deploy Instant Search Demo application to S3 Bucket '{{ instant_search_demo_old_target_bucket }}'
      local_action:
        module: community.aws.s3_sync
        endpoint_url: "{{ aws_endpoint_url | default(omit) }}"
        region: "{{ aws_region }}"
        bucket: "{{ instant_search_demo_old_target_bucket }}"
        file_root: "{{ instant_search_demo_local_artefact_path }}"
        file_change_strategy: "{{ instant_search_demo_force | ternary('force', 'date_size') }}"
        delete: true

    - name: Configure CloudFront Parameter to forward traffic to old target origin
      local_action:
        module: community.aws.aws_ssm_parameter_store
        endpoint_url: "{{ aws_endpoint_url | default(omit) }}"
        region: "{{ aws_cloudfront_region }}"
        name: "{{ ssm_current_target_parameter }}"
        value: "old"

echo "Creating AWS S3 bucket ${TERRAFORM_BACKEND_S3_BUCKET}..."
awslocal s3api create-bucket \
    --region eu-west-1 \
    --bucket ${TERRAFORM_BACKEND_S3_BUCKET} \
    --create-bucket-configuration LocationConstraint=eu-west-1

awslocal s3api put-bucket-encryption \
    --bucket ${TERRAFORM_BACKEND_S3_BUCKET} \
    --server-side-encryption-configuration "{\"Rules\": [{\"ApplyServerSideEncryptionByDefault\":{\"SSEAlgorithm\": \"AES256\"}}]}"
echo "... Finished"

echo "Creating AWS DynamoDB ${TERRAFORM_BACKEND_DYNAMODB_TABLE} table..."
awslocal dynamodb create-table \
    --region eu-west-1 \
    --table-name "${TERRAFORM_BACKEND_DYNAMODB_TABLE}" \
    --attribute-definitions AttributeName=LockID,AttributeType=S \
    --key-schema AttributeName=LockID,KeyType=HASH \
    --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5
echo "... Finished"

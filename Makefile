SHELL := /bin/bash
.SHELLFLAGS = -e -c
.ONESHELL:

.DEFAULT_GOAL: help

mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
mkfile_dir := $(dir $(mkfile_path))

.PHONY: help
help:
	@echo "Please use 'make <target>' where <target> is one of"
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//' | sed -e 's/.PHONY://'

.PHONY: instant-search-demo.prepare  ## Copy Instant Search Demo artefact to target directory ready to be deployed
instant-search-demo.prepare:
	@echo "Prepare Instant Search Demo distribution"
	@cd instant-search-demo && npm run dist

	@echo "Copy Instant Search Demo distribution to ansible target input directory for deploy:"
	@echo "(FROM) instant-search-demo/dist/ ==> (TO) infrastructure/ansible/files/instant-search-demo"
	@cd $(mkfile_dir) && rm -rf infrastructure/ansible/files/instant-search-demo/dist
	@cd $(mkfile_dir) && cp -rf instant-search-demo/dist/ infrastructure/ansible/files/instant-search-demo/dist

.PHONY: instant-search-demo.prepare-blue  ## Prepare Instant Search Demo artefact with blue CSS style override included
instant-search-demo.prepare-blue: instant-search-demo.prepare
	@echo "Include blue CSS style override"
	@cd $(mkfile_dir) && cp -f instant-search-demo/dist/assets/style-override-blue.css infrastructure/ansible/files/instant-search-demo/dist/assets/style-override.css

.PHONY: instant-search-demo.prepare-green  ## Prepare Instant Search Demo artefact with green CSS style override included
instant-search-demo.prepare-green: instant-search-demo.prepare
	@echo "Include green CSS style override"
	@cd $(mkfile_dir) && cp -f instant-search-demo/dist/assets/style-override-green.css infrastructure/ansible/files/instant-search-demo/dist/assets/style-override.css

.PHONY: instant-search-demo.prepare-red  ## Prepare Instant Search Demo artefact with red CSS style override included
instant-search-demo.prepare-red: instant-search-demo.prepare
	@echo "Include red CSS style override"
	@cd $(mkfile_dir) && cp -f instant-search-demo/dist/assets/style-override-red.css infrastructure/ansible/files/instant-search-demo/dist/assets/style-override.css

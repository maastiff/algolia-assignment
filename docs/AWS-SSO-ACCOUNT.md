# Algolia Assignment - AWS Account access through AWS SSO

This page will describe the first steps required to allow you to access to this dedicated AWS account.
Then, will be explained the several use cases you can encounter among:

- Authenticating to AWS through web browser console
- Authenticating to AWS through command line (or programmatically)

AWS SSO portal: <https://maastiff.awsapps.com/start>

## Getting Started

First, you can authenticate to this portal by creating a user account starting by the email you should have received.
You should be redirected to the authentication page below. Then, after successful authentication, you should see access
to one AWS account `maastiff-algolia` with two permission sets `AdministratorAccess` and `ViewOnlyAccess`.

![AWS SSO Portal - Authentication Page](./images/aws-sso-portal-auth.png "AWS SSO Portal - Authentication Page")

![AWS SSO Portal - Accounts List](./images/aws-sso-portal-accounts.png "AWS SSO Portal - Accounts List")

## Authenticating to AWS through web browser console

By clicking on `Management Console` of one or another permission sets, you will be redirected to AWS Console by
assuming an AWS IAM role provisioned by AWS SSO to materialize permission set into `maastiff-algolia` AWS Account.

## Authenticating to AWS through command line (or programmatically)

You can retrieve temporary credentials from AWS SSO portal, by clicking on `Command line or programmatic access` which
will open you the modal hereby:

![AWS SSO Portal - Command line or programmatc access](./images/aws-sso-portal-command-line.png "AWS SSO Portal - Command line or programmatc access")

You can choose among 3 options to set up your temporary credentials.
Note that you would need to repeat the same manual operation each time credentials expired.

You can also leverage [AWS CLI v2](https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2.html) `configure sso`
command line feature to configure authentication this account. Follow the guide below in this case.

```shell
aws configure sso --profile <target-profile>

SSO start URL [None]: https://maastiff.awsapps.com/start
SSO Region [None]: eu-west-1
Attempting to automatically open the SSO authorization page in your default browser.
If the browser does not open or you wish to use a different device to authorize this request, open the following URL:

https://device.sso.eu-west-1.amazonaws.com/

Then enter the code:

WTMP-HNXD
The only AWS account available to you is: 131605690760
Using the account ID 131605690760
There are 2 roles available to you.
Using the role name "AdministratorAccess"
CLI default client Region [None]: eu-west-1
CLI default output format [None]: json

To use this profile, specify the profile name using --profile, as shown:

aws s3 ls --profile <target-profile>
```

where `<target-profile>` is defined to `maastiff-algolia` in the rest of this documentation.

After authenticating through AWS SSO, you will get an AWS profile store in `$HOME/.aws/config`:

```ini
[profile maastiff-algolia]
sso_start_url = https://maastiff.awsapps.com/start
sso_region = eu-west-1
sso_account_id = 131605690760
sso_role_name = AdministratorAccess
region = eu-west-1
output = json
```

You just need to export `AWS_PROFILE` environment variable to be authenticated to AWS:

```shell
export AWS_PROFILE=maastiff-algolia`

aws s3 ls
2021-07-23 10:58:50 algolia-cloudfront-access-logs20210723065019688600000004
2021-07-23 08:50:33 new.instant-search-demo.algolia20210723065019682300000002
2021-07-23 08:50:32 old.instant-search-demo.algolia20210723065019682600000003
2021-07-23 08:35:47 terraform-algolia-brabant
```

When your SSO credentials would expire, you just need to refresh authentication token this way:

```shell
aws sso login
Attempting to automatically open the SSO authorization page in your default browser.
If the browser does not open or you wish to use a different device to authorize this request, open the following URL:

https://device.sso.eu-west-1.amazonaws.com/

Then enter the code:

GKRQ-MQGT
Successully logged into Start URL: https://maastiff.awsapps.com/start
```

For more information:

- <https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-sso.html>
- <https://aws.amazon.com/fr/blogs/security/aws-single-sign-on-now-enables-command-line-interface-access-for-aws-accounts-using-corporate-credentials/>

WARNING: AWS SSO is a quite recent feature, thus SDK support for authentication using AWS SSO token is quite sparse,
even completely unsupported for old SDK like boto.

See troubleshooting section for more information.

## Troubleshooting

### Profile given for AWS was not found

When running `ansible-playbook create-terraform-backend.yaml`,
ansible fail at step `Create terraform backend DynamoDB table` with the following error message:

> Profile given for AWS was not found.  Please fix and retry.

This is probably due to
[community.aws.dynamodb_table](https://docs.ansible.com/ansible/latest/collections/community/aws/dynamodb_table_module.html)
ansible module that rely on outdated SDK which does not support authentication from SSO token.

=> In this case it is better to directly rely on STS credentials directly retrieved from AWSS SSO portal

```shell
export AWS_ACCESS_KEY_ID="ASIAR5JCPPGEP5JTUKYY"
export AWS_SECRET_ACCESS_KEY="AgjIJBPV7pVrgEQQicKSXSv2ykk8VNkUqVNIVQCP"
export AWS_SESSION_TOKEN="IQoJb3JpZ2luX2VjEAQaCWV1LXdlc3QtMSJIMEYCIQDj2g6yR1vgYYTE...bp3/4Aifv2QBdWtBx"
```

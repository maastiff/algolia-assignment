import chaiColors from 'chai-colors'
chai.use(chaiColors)

const COLOR_MAPPING = {
    "DEFAULT": "#ffffff",
    "WHITE": "#ffffff",
    "BLUE": "#5568ff",
    "GREEN": "#00e676",
    "RED": "#ff426b",
}
const CLOUDFRONT_ORIGINS_MAPPING = ["old", "new"];

const SEARCH_BAR_COLOR = Cypress.env('SEARCH_BAR_COLOR').toUpperCase()
const FORCE_CLOUDFRONT_ORIGIN = Cypress.env('FORCE_CLOUDFRONT_ORIGIN') ? Cypress.env('FORCE_CLOUDFRONT_ORIGIN').toLowerCase() : undefined

if (!(SEARCH_BAR_COLOR in COLOR_MAPPING)) {
    throw new Error(`Provided SEARCH_BAR_COLOR argument '${SEARCH_BAR_COLOR}' is NOT expected (expect one of: ${Object.keys(COLOR_MAPPING)})`)
}

if (FORCE_CLOUDFRONT_ORIGIN && !CLOUDFRONT_ORIGINS_MAPPING.includes(FORCE_CLOUDFRONT_ORIGIN)) {
    throw new Error(`Provided FORCE_CLOUDFRONT_ORIGIN argument '${FORCE_CLOUDFRONT_ORIGIN}' is NOT expected (expect one of: ${CLOUDFRONT_ORIGINS_MAPPING})`)
}

Cypress.Cookies.defaults({
    preserve: 'X-CloudFront-Force-Current-Target',
})

context('Assertions', () => {
    beforeEach(() => {

        if (FORCE_CLOUDFRONT_ORIGIN) {
            cy.setCookie('X-CloudFront-Force-Current-Target', FORCE_CLOUDFRONT_ORIGIN)
        }

        cy.visit('/', {
            "headers": {
                "Cache-Control": "no-cache"
            }
        })

        cy.get('#search-input input[type="search"]').as("search_bar")
        cy.get('#hits li.ais-Hits-item').as('search_result')
    })

    describe('Instant Search Demo', () => {
        it('should return Bokoblin amiibo figure, when searching Bokoblin', () => {
            cy.get('@search_bar')
                .type('Bokoblin')
                .should('have.value', 'Bokoblin')

            cy.get('@search_result')
                .should('have.length', 1)
            cy.get('@search_result').first()
                .should('contain', 'Nintendo - amiibo Figure (The Legend of Zelda: Breath of the Wild Series Bokoblin)')
        })

        it(`should display search bar in "${SEARCH_BAR_COLOR}" color`, () => {
            cy.get('@search_bar')
                .should('have.css', 'background-color')
                .and('be.colored', COLOR_MAPPING[SEARCH_BAR_COLOR])
        })
    })
})

window.env = {
    appId: '${ALGOLIA_APP_ID}',
    apiKey: '${ALGOLIA_API_KEY}',
    indexName: '${ALGOLIA_INDEX_NAME}',
}
